<?php

	$task = ['Get git', 'Bake HTML', 'Eat CSS', 'Learn PHP'];

	if (isset($_GET['index'])) {
		$indexGET = $_GET['index'];
		echo "The retrieved task from GET is $task[$indexGET]. ";
	}

	if (isset($_POST['index'])) {
		$indexPOST = $_POST['index'];
		echo "The retrieved task from POST is $task[$indexPOST]. ";
	}

?>





<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S05: Client-Server Communication (Get and Post)</title>
</head>
<body>
	<h1>Task index from GET</h1>

	<form method="GET">
		<select name="index" required>
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>

		<button type="submit">GET</button>

	</form>

	<h1>Task index from POST</h1>

	<form method="POST">
		<select name="index" required>
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>
		<button type="submit" style="color: red; border-radius: 1px; background: blue;">POST</button>
	</form>

</body>
</html>